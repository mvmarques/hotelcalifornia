const mongoose = require('../database');

 const EnderecoSchema = new mongoose.Schema({
    cep: String,
    city: String,
    neighborhood: String,
    state: String,
    street: String,
    createAt: Date,
    updatedAt: Date,
});

module.exports = mongoose.model('Endereco', EnderecoSchema);