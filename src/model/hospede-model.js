
const mongoose = require('../database');

 const HospedeSchema = new mongoose.Schema({
    nome: String,
	sobrenome: String,
	idade: Number,
	email: String,     
    cpf: String,
    createAt: Date,
    updatedAt: Date,
	endereco: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Endereco',
        required: false
    }
});

module.exports = mongoose.model('Hospede', HospedeSchema);