const mongoose = require('../database');

 const ReservaSchema = new mongoose.Schema({
    checkin: Date,
    checkout: Date,
    createAt: Date,
    updatedAt: Date,
	hospede: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Hospede'
    },
    quarto: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Quarto'
    },
});

module.exports = mongoose.model('Reserva', ReservaSchema);