const mongoose = require('../database');

const QuartoSchema = new mongoose.Schema({
    thumbnail: {
        type: String,
        required: false
    },
    numero: String,
    andar: Number,
    categoria: String,
    classificacao: Number,
    preco: {
        type: Number,
        required : false
    },
    createAt: Date,
    updatedAt: Date,
}, {
    toJSON: {virtuals: true}
});

QuartoSchema.virtual('thumbnail_url').get(function() {
    return `http://localhost:3131/files/${this.thumbnail}`
});

module.exports = mongoose.model('Quarto', QuartoSchema);