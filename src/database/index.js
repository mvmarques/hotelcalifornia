const mongoose = require('mongoose');

mongoose.connect('mongodb://admin:123456a@ds141208.mlab.com:41208/hotelcalifornia', { useUnifiedTopology: true });

mongoose.Promise = global.Promise;
console.log(mongoose.connection.readyState);

module.exports = mongoose;