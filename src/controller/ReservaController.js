const Reserva = require('../model/reserva-model');
const Quarto = require('../model/quarto-model');
const Hospede = require('../model/hospede-model');
module.exports = {

    async index(req, res) {
        const reserva = await Reserva.find();

        if(reserva) {
            return res.status(200).json(reserva);
        }
        
        return res.status(200).json('Sem dados !');
    }, 

    async show(req, res) {
        
        const { object_id } = req.body;
        //
    },

    async store(req, res) {

        const { checkin, checkout, hospede_id, quarto_id } = req.body;
    
        const hospede = Hospede.findById(hospede_id);
        const quarto = Quarto.findById(quarto_id);

        const reserva = Reserva.create({ checkin, checkout, hospede, quarto });
        
        res.status(200).json(reserva)
    
    },

    async update(req, res) {

    },

    async destroy(req, res) {

        const { reserva_id } = req.body;

        Reserva.findByIdAndDelete(reserva_id)
    }


}