const Hospede = require('../model/hospede-model');
const Endereco = require('../model/endereco-model');

module.exports = {

    async index(req, res) {

        const hospedes = await Hospede.find();

        return res.status(200).json(hospedes);

    }, 

    async show(req, res) {

        // Categoria, Classificação, Disponiveis, Preço
        const { cpf } = req.body;

        const hospede = await Hospede.findOne({ cpf });
        
        if (hospede){
            return res.status(200).json(hospede);
        }
        
        return res.status(200).json('Nenhum usuário encontrado !');
    },

    async store(req, res) {
     
        const { nome, sobrenome, idade,  email, 
                cpf, cep, rua, bairro, cidade  } = req.body;

         let hospede = await Hospede.findOne({cpf}); 
         
         if (!hospede) {
             
             let endereco = await Endereco.create({ cep, rua, bairro, cidade  });

             hospede = await Hospede.create({ nome, sobrenome, idade, email, cpf, endereco });
            
         }

         res.json(hospede);

            
    },

    async update(req, res) {

        const { nome, sobrenome, idade,  email, 
            cpf, cep, rua, bairro, cidade  } = req.body;

        const hospede = await Hospede.findOneAndUpdate({ nome, sobrenome, idade, email, cpf, cep, rua, bairro , cidade });
            
        return res.json(hospede);
    },

    async destroy(req, res) {

        const { cpf } = req.body;

        const hospede = await Hospede.findOne({ cpf });
        
        if( hospede ) {
            Hospede.remove({ cpf });
             res.status(200).json('Registro Deletado!')
        }
            res.status(200).json('Não há registro para deletar !')
       
    }


}