const Quarto = require('../model/quarto-model');
// index, show, store, update, destroy
module.exports = {

    async index(req, res) {
       
        const quartos = await Quarto.find();

        if(quartos) {
        await quartos.map(( response ) => {

            const SOLTEIRO = 49;
            const DUPLO = 59;
            const FAMILIA = 89;
            
        
            if (response.categoria === 'SOLTEIRO') {
                response.preco =  SOLTEIRO * response.classificacao;
            } else if(response.categoria === 'DUPLO') {
                response.preco =  DUPLO * response.classificacao;
            } else if(response.categoria === 'FAMILIA') {
                response.preco =  FAMILIA * response.classificacao;
            }
            
        });
        
            return res.status(200).json(quartos);
        }

       return res.status(200).json('Sem dados !');
    }, 

    async show(req, res) {

       const { numero, andar, preco, classificacao, categoria } = req.body;

        const quarto = await Quarto.findOne({ numero, andar, categoria }); 

        if (quarto) {
            return res.status(200).json(quarto);
        }

        return res.status(200).json('Quarto não encontrado!');
    },

    async store(req, res) {

        const { filename } = req.file;

        const { numero, andar, categoria, classificacao } =  req.body;
    
        const quarto = await Quarto.create({ 
            thumbnail: filename,
            numero : numero, 
            andar : andar, 
            categoria : categoria, 
            classificacao : classificacao
        })
        
        res.status(200).json(quarto);
    },

    async update(req, res) {
       
        const { numero, andar, categoria, classificacao } =  req.body;
    
        const quarto = await Quarto.findByIdAndUpdate({ numero, andar, categoria, classificacao })
        
        res.status(200).json(quarto);
    },

    async destroy(req, res) {

        const { object_id } = req.body;

        const quarto = await Quarto.findOneAndDelete(object_id);

        res.status(200).json(quarto);

    },

    // it needs to be refatorated 
    precificar ( categoria, classificacao ) {
        const SOLTEIRO = 49;
        const DUPLO = 59;
        const FAMILIA = 89;

        if (categoria === 'SOLTEIRO') {
            return SOLTEIRO * classificacao;
        } else if(categoria === 'DUPLO') {
            return DUPLO * classificacao;
        } else if(categoria === 'FAMILIA') {
            return FAMILIA * classificacao;
        }
        
    }


}