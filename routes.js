const express = require('express');
const multer = require('multer');
const uploadConfig = require('./src/config/upload');

const QuartoController = require('./src/controller/QuartoController');
const HospedeController = require('./src/controller/HospedeController');
const ReservaController = require('./src/controller/ReservaController');

const routes = express.Router();
const upload = multer(uploadConfig);

routes.get('/', (req, res) => {
    res.send('OK')
});

routes.get('/quartos', QuartoController.index);
routes.get('/quarto/obter', QuartoController.show);
routes.post('/quarto/salvar', upload.single('thumbnail'),  QuartoController.store);
routes.delete('/quarto/delete', QuartoController.destroy);
routes.put('/quarto/editar', QuartoController.update);

routes.get('/hospede', HospedeController.index);
routes.get('/hospede/obter', HospedeController.show);
routes.post('/hospede/salvar', HospedeController.store);
routes.delete('/hospede/deletar', HospedeController.destroy);
routes.put('/hospede/editar', HospedeController.update);

routes.get('/reserva', ReservaController.index);
routes.post('/reserva/salvar', ReservaController.store);
routes.delete('/reserva/deletar', ReservaController.destroy);
routes.put('/reserva/editar', ReservaController.update);

module.exports = routes;

